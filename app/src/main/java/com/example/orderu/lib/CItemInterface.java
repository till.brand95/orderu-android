package com.example.orderu.lib;

import java.io.Serializable;

public class CItemInterface extends CObject {
    public CItemInterface(long _nativeHandle) {
        super(_nativeHandle);
    }
    public native String getId();
    public native String getName();
    public native void setName(String name);
}
