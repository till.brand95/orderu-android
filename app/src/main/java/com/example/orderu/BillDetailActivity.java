package com.example.orderu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.orderu.lib.CBill;
import com.example.orderu.lib.CItem;
import com.example.orderu.lib.CObject;
import com.example.orderu.lib.COrder;
import com.example.orderu.lib.CustomAppActivity;

public class BillDetailActivity extends CustomAppActivity {
    TableLayout tl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail);
        Intent myIntent = getIntent();
        CObject obj = myIntent.getParcelableExtra("bill");
        CBill bill = new CBill(obj.getNativeHandle());
        tl = findViewById(R.id.billd_detail_table);
        tl.setColumnStretchable(0,true);
        tl.setColumnShrinkable(1,true);
        TextView time = findViewById(R.id.bill_time_fied);
        TextView table = findViewById(R.id.bill_table_field);
        TextView total = findViewById(R.id.bill_total_field);

        time.setText(String.valueOf(bill.getTime()));
        table.setText(String.valueOf(bill.getTable()));
        total.setText(String.valueOf(bill.getTotal()));

        COrder[] items = bill.getItems();

        for(int i = 0; i < items.length; i++)
        {
            CItem it = items[i].getItem();
            TableRow tr = new TableRow(this);
            TextView tv1 = new TextView(this);
            tv1.setTextSize(18);
            tv1.setText(it.getName());
            TextView tv2 = new TextView(this);
            tv2.setTextSize(18);
            String price = String.valueOf(it.getPrice())+"€";
            tv2.setText(price);
            tr.addView(tv1);
            tr.addView(tv2);
            tl.addView(tr);
        }

        TableRow tr = new TableRow(this);
        TextView tv1 = new TextView(this);
        tv1.setTextSize(24);
        tv1.setText("Total");
        TextView tv2 = new TextView(this);
        tv2.setTextSize(24);
        String price = String.valueOf(bill.getTotal())+"€";
        tv2.setText(price);
        tr.addView(tv1);
        tr.addView(tv2);
    }


}
