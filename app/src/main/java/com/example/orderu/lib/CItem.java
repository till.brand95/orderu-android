package com.example.orderu.lib;

public class CItem extends CItemInterface {

    public CItem(long _nativeHandle) {
        super(_nativeHandle);
    }

    public native float getPrice();
    public native void setPrice(float price);
    public native static CItem newItem();

}
