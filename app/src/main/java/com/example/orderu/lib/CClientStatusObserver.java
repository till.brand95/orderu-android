package com.example.orderu.lib;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.example.orderu.MainActivity;
import com.example.orderu.OrderUApp;
import com.example.orderu.R;

import java.util.Observable;
import java.util.Observer;

public class CClientStatusObserver implements Observer {

    private static CClientStatusObserver instance;
    public static CClientStatusObserver getInstance () {
        if (CClientStatusObserver.instance == null) {
            CClientStatusObserver.instance = new CClientStatusObserver ();
        }
        return CClientStatusObserver.instance;
    }

    public void observe(Observable o) {
        o.addObserver(getInstance ());
    }
    @Override
    public void update(Observable o, Object arg) {
        if(!CClientStatus.isConnectedSyncronized())
        {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    ConnectionLostDialigBuilder.getDialog().show();
                }
            });
        }
    }
}
