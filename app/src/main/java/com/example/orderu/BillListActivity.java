package com.example.orderu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.orderu.lib.CBill;
import com.example.orderu.lib.CObject;
import com.example.orderu.lib.CustomAppActivity;

import java.nio.channels.ClosedByInterruptException;

import static com.example.orderu.lib.CManager.getBills;

public class BillListActivity extends CustomAppActivity {
    private static final int REQUEST_EXIT = 100;
    TableLayout rec_table;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_orders);

        rec_table = findViewById(R.id.receipe_list);

        rec_table.setColumnStretchable(0, true);
        rec_table.setColumnStretchable(1, true);
        rec_table.setColumnShrinkable(2, true);
        CBill[] bills = getBills();

        for(int i =  bills.length -1; i >= 0; i--)
        {
            final TableRow tr = new TableRow(this);


            TextView tv1 = new TextView(this);tv1.setTextSize(25);
            TextView tv2 = new TextView(this);tv2.setTextSize(25);
            TextView tv3 = new TextView(this);tv3.setTextSize(25);

            tv1.setText(String.valueOf(bills[i].getTime()));
            tv2.setText(String.valueOf(bills[i].getTable()));
            String price = String.valueOf(bills[i].getTotal()) + "€";
            tv3.setText(price);
            tr.addView(tv1);tr.addView(tv2);tr.addView(tv3);
            tr.setTag(bills[i]);
            tr.setMinimumHeight(20);

            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TableRow tr = (TableRow) v;
                    Intent intent = new Intent(BillListActivity.this, BillDetailActivity.class);
                    intent.putExtra("bill",new CBill(((CObject)tr.getTag()).getNativeHandle()));
                    startActivityForResult(intent, REQUEST_EXIT);
                }
            });

            rec_table.addView(tr);

        }
    }
}
