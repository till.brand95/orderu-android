package com.example.orderu.lib;

import android.os.Parcel;
import android.os.Parcelable;

public class CObject implements Parcelable {
    protected long nativeHandle = 0;
    public CObject(long _nativeHandle) {
        nativeHandle = _nativeHandle;
    }

    protected CObject(Parcel in) {
        nativeHandle = in.readLong();
    }

    public long getNativeHandle() {
        return nativeHandle;
    }

    public void setNativeHandle(long nativeHandle) {
        this.nativeHandle = nativeHandle;
    }

    public static final Creator<CObject> CREATOR = new Creator<CObject>() {
        @Override
        public CObject createFromParcel(Parcel in) {
            return new CObject(in);
        }

        @Override
        public CObject[] newArray(int size) {
            return new CObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(nativeHandle);
    }
}
