package com.example.orderu.lib;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class CClientStatus extends Observable {

    public static void addObserverS(Observer o)
    {
        getInstance().get().addObserver(o);
    }
    private AtomicBoolean connected = new AtomicBoolean(false);
    private static AtomicReference<CClientStatus> instance;
    private CClientStatus()
    {
    }

    public static AtomicReference<CClientStatus> getInstance () {
        if (CClientStatus.instance == null) {
            CClientStatus.instance = new AtomicReference<> (new CClientStatus());
        }
        return CClientStatus.instance;
    }

    public static void setClientStatus(boolean status)
    {
        synchronized (getInstance ()) {
            getInstance ().get().connected.set(status);
        }

        getInstance ().get().setChanged();
        getInstance ().get().notifyObservers();
    }

    public static synchronized boolean isConnectedSyncronized()
    {
        return getInstance ().get().connected.get();
    }

    public native static boolean isConnected();
}
