package com.example.orderu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.example.orderu.lib.CClientStatus;
import com.example.orderu.lib.CClientStatusObserver;
import com.example.orderu.lib.ConnectionLostDialigBuilder;

import java.util.concurrent.atomic.AtomicReference;



public class OrderUApp extends Application {
    public static final int CONNECTION_STATE = 100;

    private Activity mCurrentActivity = null;
    private static OrderUApp appContext;
    @Override
    public void onCreate () {
        super .onCreate();
        appContext = this;
        CClientStatusObserver.getInstance().observe(CClientStatus.getInstance().get());
    }

    public static OrderUApp getAppContext() {
        return appContext;
    }

    public Activity getCurrentActivity () {
        return mCurrentActivity ;
    }
    public void setCurrentActivity (Activity mCurrentActivity) {
        this . mCurrentActivity = mCurrentActivity;
    }
}
