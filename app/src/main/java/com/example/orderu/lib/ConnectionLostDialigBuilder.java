package com.example.orderu.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.orderu.MainActivity;
import com.example.orderu.OrderUApp;
import com.example.orderu.ui.login.LoginActivity;

import java.util.concurrent.TimeUnit;

import static com.example.orderu.lib.CManager.performLogin;
import static com.example.orderu.lib.CManager.startClient;

public class ConnectionLostDialigBuilder {

    public static Dialog getDialog()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                OrderUApp.getAppContext().getCurrentActivity());

        alertDialogBuilder.setTitle("Conection to Server Lost!");
        alertDialogBuilder
                .setMessage("Retry?")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        if(CManager.retryConnect())
                        {

                            performLogin(LoginActivity.Code);
                            if (CClientStatus.isConnectedSyncronized())
                            {
                                Activity ma = OrderUApp.getAppContext().getCurrentActivity();
                                if(ma instanceof MainActivity)
                                {
                                    MainActivity.updateTables();
                                }
                                dialog.cancel();
                                return;
                            }
                        }
                        ConnectionLostDialigBuilder.getDialog().show();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                });
        return alertDialogBuilder.create();
    }

}
