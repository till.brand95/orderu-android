package com.example.orderu.lib;

public class COrder extends CItemInterface {
    public COrder(long _nativeHandle) {
        super(_nativeHandle);
    }

    public native CItem getItem();
    public native void setItem(CItem item);
    public native CItem[] getSubItems();
    public native void setSubItem(CItem[] items);
    public native static COrder newOrder();
}
