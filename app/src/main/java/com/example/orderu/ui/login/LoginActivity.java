package com.example.orderu.ui.login;

import android.app.Activity;

import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.orderu.MainActivity;
import com.example.orderu.R;
import com.example.orderu.lib.CClientStatus;
import com.example.orderu.lib.CManager;
import com.example.orderu.lib.ConnectionLostDialigBuilder;
import com.example.orderu.lib.CustomAppActivity;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import static com.example.orderu.lib.CManager.performLogin;
import static com.example.orderu.lib.CManager.startClient;

public class LoginActivity extends CustomAppActivity {
    ProgressBar loadingProgressBar;
    Button loginButton;
    EditText passwordEditText;
    EditText ip_edit;
    AlertDialog alertDialog;

    public static int Code=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        passwordEditText = findViewById(R.id.password);
        loginButton = findViewById(R.id.login);
        loadingProgressBar  = findViewById(R.id.loading);
        ip_edit  = findViewById(R.id.ip_address_edit);
        Button connectButton = findViewById(R.id.button_set_ip);

        //clientStatus = CClientStatus.getInstance();

        connectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startClient(ip_edit.getText().toString());
            }
        });

        loadingProgressBar.setVisibility(View.INVISIBLE);



        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String passwd = passwordEditText.getText().toString();
                Code = Integer.valueOf(passwd);
                if(!CClientStatus.isConnectedSyncronized())
                {
                    ConnectionLostDialigBuilder.getDialog().show();
                    return;
                }
                loadingProgressBar.setVisibility(View.VISIBLE);

                boolean loggedin = performLogin(Code);
                if(loggedin == true)
                {
                    loadingProgressBar.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                else
                {
                    loadingProgressBar.setVisibility(View.INVISIBLE);
                    TextView tv = findViewById(R.id.error_text);
                    tv.setText("Login Failed!");
                }

            }
        });
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    public void initAlertDialog()
    {

    }




}
