package com.example.orderu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.gridlayout.widget.GridLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.example.orderu.lib.CItem;
import com.example.orderu.lib.CObject;
import com.example.orderu.lib.CTable;
import com.example.orderu.lib.CustomAppActivity;

import java.util.ArrayList;
import java.util.List;

import static com.example.orderu.lib.COrderuClient.addOrderList;
import static com.example.orderu.lib.CManager.getItems;

public class OrderActivity extends CustomAppActivity {

    private CTable _table;
    TableLayout lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        GridLayout items = findViewById(R.id.grid_items);

        Intent myIntent = getIntent(); // gets the previously  created intent
        CObject obj = myIntent.getParcelableExtra("table");
        _table = new CTable(obj.getNativeHandle());

        CItem[] all_items = getItems();

        lv = findViewById(R.id.order_list);

        for(final CItem cja : all_items)
        {
            Button btn = new Button(this);
            btn.setText(cja.getName());
            btn.setTag(new CItem(cja.getNativeHandle()));
            btn.setHeight(100);
            btn.setWidth(200);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final TableRow tr = new TableRow(OrderActivity.this);
                    Button b = (Button) v;
                    CItem item = (CItem) b.getTag();
                    TextView tv1 = new TextView(OrderActivity.this);tv1.setTextSize(25);
                    TextView tv2 = new TextView(OrderActivity.this);tv2.setTextSize(25);
                    tr.setTag(b.getTag());
                    tv1.setText(item.getName());
                    tv2.setText(String.valueOf(item.getPrice()));
                    tr.setMinimumHeight(20);
                    tr.addView(tv1);
                    tr.addView(tv2);
                    tr.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View vvv) {
                            lv.removeView(vvv);
                        }
                    });
                    lv.addView(tr);
                }
            });
            items.addView(btn);

        }

        Button send_button = findViewById(R.id.send_button);
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<CItem> items = new ArrayList<CItem>();
                for(int i = 0; i < lv.getChildCount(); i++)
                {
                    TableRow tr = (TableRow) lv.getChildAt(i);
                    CItem cji = (CItem) tr.getTag();
                    if(cji != null)
                        items.add(cji);
                }
                CItem[] itemsArray = new CItem[items.size()];
                addOrderList(items.toArray(itemsArray),_table.getNumber());
                setResult(RESULT_OK, null);
                OrderActivity.this.finish();
            }
        });





    }

}
