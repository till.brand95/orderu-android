package com.example.orderu.lib;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.orderu.OrderUApp;

import java.util.concurrent.atomic.AtomicReference;


public abstract class CustomAppActivity extends AppCompatActivity {
    protected void onCreate (Bundle savedInstanceState) {
        super .onCreate(savedInstanceState) ;
        OrderUApp.getAppContext().setCurrentActivity( this ) ;
    }
    protected void onResume () {
        super .onResume() ;
        OrderUApp.getAppContext().setCurrentActivity( this ) ;
    }
    protected void onPause () {
        clearReferences() ;
        super .onPause() ;
    }
    protected void onDestroy () {
        clearReferences() ;
        super .onDestroy() ;
    }
    protected void clearReferences () {
        Activity currActivity =  OrderUApp.getAppContext() .getCurrentActivity() ;
        if ( this .equals(currActivity))
            OrderUApp.getAppContext() .setCurrentActivity( null ) ;
    }
}
