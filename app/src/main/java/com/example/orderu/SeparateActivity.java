package com.example.orderu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.orderu.lib.CItem;
import com.example.orderu.lib.CObject;
import com.example.orderu.lib.COrder;
import com.example.orderu.lib.CTable;
import com.example.orderu.lib.CustomAppActivity;

import static com.example.orderu.lib.COrderuClient.addBill;

public class SeparateActivity extends CustomAppActivity {
    CTable _table;
    TableLayout separate_table;
    TableLayout pay_table;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_separate);
        final Intent myIntent = getIntent(); // gets the previously  created intent
        CObject obj = myIntent.getParcelableExtra("table");
        _table = new CTable (obj.getNativeHandle());
        separate_table = findViewById(R.id.separate_list);
        pay_table = findViewById(R.id.buy_table);
        separate_table.setColumnStretchable(0, true);
        pay_table.setColumnStretchable(0, true);
        COrder[] orders = _table.getOrders();

        for(int i = 0; i < orders.length; i++)
        {
            final TableRow tr = new TableRow(this);
            TextView tv1 = new TextView(this);tv1.setTextSize(25);
            TextView tv2 = new TextView(this);tv2.setTextSize(25);

            CItem item = orders[i].getItem();
            tv1.setText(item.getName());
            tv2.setText(String.valueOf(item.getPrice()));
            tr.setMinimumHeight(20);
            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vvv) {
                    OnClickSeparate(tr);
                }
            });
            tr.setTag(orders[i]);
            tr.addView(tv1);
            tr.addView(tv2);
            separate_table.addView(tr);

        }
        Button separate_button = findViewById(R.id.button_separate);

        separate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View mv) {
                COrder[] orders = new COrder[pay_table.getChildCount()];

                for( int i = 0;  i < pay_table.getChildCount(); i++)
                {
                    TableRow tr = (TableRow) pay_table.getChildAt(i);
                    orders[i] = (COrder) tr.getTag();
                }

                addBill(orders, _table.getNumber());

                pay_table.removeAllViews();
                if(separate_table.getChildCount() == 0)
                {
                    setResult(RESULT_OK, null);
                    SeparateActivity.this.finish();
                }
            }
        });

    }

    void OnClickSeparate(final TableRow v)
    {
        Switch sw = findViewById(R.id.separate_all);
        if(sw.isChecked()) {
            for( int i = separate_table.getChildCount()-1;  i >= 0; i--) {
                TableRow vv = (TableRow) separate_table.getChildAt(i);
                vv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View vvv) {
                        OnClickBuy(v);
                    }
                });
                separate_table.removeView(vv);
                pay_table.addView(vv);

            }
        }
        else
        {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vvv) {
                    OnClickBuy( v);
                }
            });
            separate_table.removeView(v);
            pay_table.addView(v);
        }
    }

    void OnClickBuy(final TableRow v)
    {
        Switch sw = findViewById(R.id.separate_all);
        if(sw.isChecked()) {
            for( int i = pay_table.getChildCount()-1;  i >= 0; i--) {
                TableRow vv = (TableRow) pay_table.getChildAt(i);
                vv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View vvv) {
                        OnClickSeparate(v);
                    }
                });
                pay_table.removeView(vv);
                separate_table.addView(vv);
            }
        }
        else
        {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vvv) {
                    OnClickSeparate(v);
                }
            });
            pay_table.removeView(v);
            separate_table.addView(v);

        }
    }
}
