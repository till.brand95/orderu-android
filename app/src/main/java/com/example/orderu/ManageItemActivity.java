package com.example.orderu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.orderu.lib.CItem;
import com.example.orderu.lib.CustomAppActivity;

import static com.example.orderu.lib.COrderuClient.addItemList;

public class ManageItemActivity extends CustomAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_item);

        Button btn = findViewById(R.id.button_add_item);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CItem item = CItem.newItem();
                EditText price =  findViewById(R.id.item_price_edit);
                EditText name =  findViewById(R.id.item_name_edit);
                item.setPrice(Double.valueOf(price.getText().toString()).floatValue());
                item.setName(name.getText().toString());
                CItem[] items = new CItem[1];
                items[0] = item;
                addItemList(items);
                finish();
            }
        });
    }
}
