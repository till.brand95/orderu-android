package com.example.orderu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.gridlayout.widget.GridLayout;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.example.orderu.lib.CClientStatus;
import com.example.orderu.lib.CManager;
import com.example.orderu.lib.CObject;
import com.example.orderu.lib.CTable;
import com.example.orderu.lib.CWaiter;
import com.example.orderu.lib.CustomAppActivity;


import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import static com.example.orderu.lib.CManager.getTables;
import static com.example.orderu.lib.CManager.retryConnect;
import static com.example.orderu.lib.CWaiter.*;

public class MainActivity extends CustomAppActivity {

    // Used to load the 'native-lib' library on application startup.

    static GridLayout _table_grid;
    static MainActivity ma;

    static Switch sw;

    static final int REQUEST_EXIT = 10090;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ma = this;

        _table_grid = findViewById(R.id.table_control);

        sw = findViewById(R.id.switch_separieren);
        init();

        updateTables();

        Button bb = findViewById(R.id.button_bills);

        bb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BillListActivity.class);
                startActivityForResult(intent, REQUEST_EXIT);
            }
        });
        if(CWaiter.hasPermission(CWaiter.UP_ITEM, CWaiter.PT_ALL)) {
            Button new_item = findViewById(R.id.button_add_new_item);
            new_item.setVisibility(View.INVISIBLE);
        }
    }

    public static void addTable(CTable table)
    {
        Button btn = new Button(ma);
        btn.setText(String.valueOf(table.getNumber()));
        btn.setHeight(100);
        btn.setWidth(200);
        btn.setTag(new CTable(table.getNativeHandle()));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sw.isChecked()) {
                    Intent intent = new Intent(ma, OrderActivity.class);
                    Button b = (Button) v;
                    CTable tab = (CTable)b.getTag();
                    intent.putExtra("table", tab);
                    ma.startActivityForResult(intent, REQUEST_EXIT);
                }
                else
                {
                    Intent intent = new Intent(ma, SeparateActivity.class);
                    Button b = (Button) v;
                    intent.putExtra("table", (CTable)b.getTag());
                    ma.startActivityForResult(intent, REQUEST_EXIT);
                }
            }
        });
        _table_grid.addView(btn);
    }

    public static void removeTable(CTable t)
    {
        int size =_table_grid.getChildCount();
        for(int i = 0; i < size; i++)
        {
            Button b = (Button) _table_grid.getChildAt(i);
            CObject obj = (CObject) b.getTag();
            CTable table = new CTable(obj.getNativeHandle());
            if(t.getNumber() == table.getNumber())
                _table_grid.removeView(b);
        }
    }

    public static void updateTables() {
        CTable[] tables = getTables();
        _table_grid.removeAllViews();
        for(int i = 0; i < tables.length; i++)
        {
            addTable(tables[i]);
        }

        Button add_new_table = ma.findViewById(R.id.button_add_new_table);
        add_new_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ma, OrderActivity.class);
                EditText edit_new_table_number = ma.findViewById(R.id.edit_new_table_number);
                CTable table = CTable.newTable(Integer.parseInt(edit_new_table_number.getText().toString()));
                intent.putExtra("table", table);
                ma.startActivityForResult(intent, REQUEST_EXIT);

            }
        });
        Button view_bills = ma.findViewById(R.id.button_bills);
        view_bills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ma, BillListActivity.class);
                ma.startActivityForResult(intent, REQUEST_EXIT);
            }
        });

        Button add_new_item = ma.findViewById(R.id.button_add_new_item);
        add_new_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ma, ManageItemActivity.class);
                ma.startActivityForResult(intent, REQUEST_EXIT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        updateTables();
    }

    public native static void init();

}
