//
// Created by Till on 27/10/2019.
//

#ifndef ORDERU_CLIENTHANDLER_H
#define ORDERU_CLIENTHANDLER_H

#include <jni.h>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <atomic>
#include "shared/orderu_client.h"
#include "helpers.h"

using namespace orderU;

class ClientHandler
{
public:
    ClientHandler()
    {
        _client = std::make_shared<orderU::orderu_client>();


        // Call the method on the object
    }
    ~ClientHandler()
    {
        _client->stop();
    }
    void init(const char *ip)
    {
        if(initialized)
            return;

        _client->create(ip);
        _client->getService()->onConnect.push_back([&](const boost::system::error_code & e)
                                                   {
                                                       _connected = true;
                                                   });
        _client->getService()->onDisconnect.push_back([&](connection_ptr conn)
                                                      {
                                                          _connected = false;
                                                      });

        initialized=true;

    }
    void start()
    {
        _client->run();
    }

    bool initialized= false;



    bool isConnected(){return _connected;}

    bool _connected =false;


    std::shared_ptr<orderU::orderu_client> _client;
private:


};

#endif //ORDERU_CLIENTHANDLER_H
