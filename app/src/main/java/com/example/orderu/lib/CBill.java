package com.example.orderu.lib;

public class CBill extends CItemInterface {
    public CBill(long _nativeHandle) {
        super(_nativeHandle);
    }

    public native float getTotal();
    public native COrder[] getItems();
    public native int getTime();
    public native int getTable();
    public native static CBill newBill();


}
