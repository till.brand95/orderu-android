package com.example.orderu.lib;

import java.util.Observable;

public class CManager  {

    static {
        System.loadLibrary("native-lib");
    }

    static CClientStatusObserver observer = new CClientStatusObserver();
    static CClientStatus clientStatus;

    public static void init()
    {
        observer.observe(clientStatus);
    }

    public static native CTable[] getTables();
    public static native CBill[] getBills();
    public static native CItem[] getItems();
    public native static boolean performLogin(int jStr);
    public native static boolean startClient(String ip);
    public native static boolean retryConnect();





}
