package com.example.orderu.lib;

public class CWaiter {

    public final static int   UP_ITEM = 101;
    public final static int  UP_ORDER = 102;
    public final static int  UP_BILL = 103;
    public final static int  UP_TABLE = 104;
    public final static int  UP_USER = 105;

    public final static int PT_NONE = 0;
    public final static int PT_READ = 1;
    public final static int PT_UPDATE = 10;
    public final static int PT_REMOVE = 100;
    public final static int PT_ALL = PT_READ | PT_UPDATE | PT_REMOVE;

    public static native boolean hasPermission(int up, int ut);
}
