package com.example.orderu.lib;

public class CTable extends CItemInterface {
    public CTable(long _nativeHandle) {
        super(_nativeHandle);
    }

    public native int getNumber();
    public native void setNumber(double number);
    public native COrder[] getOrders();
    public native static CTable newTable(int number);
}
