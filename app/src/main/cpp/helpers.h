//
// Created by Till on 22/10/2019.
//

#ifndef ORDERU_HELPERS_H
#define ORDERU_HELPERS_H

#include <jni.h>
#include <memory>


jfieldID getHandleField(JNIEnv *env, jobject obj)
{
    jclass c = env->GetObjectClass(obj);
    // J is the type signature for long:
    return env->GetFieldID(c, "nativeHandle", "J");
}
template<typename T>
std::shared_ptr<T>& getHandle(JNIEnv *env, jobject obj)
{
    jlong handle = env->GetLongField(obj, getHandleField(env, obj));
    std::shared_ptr<T>* tmp = reinterpret_cast<std::shared_ptr<T>*>(handle);
    return *tmp;
}

void setHandle(JNIEnv *env, jobject obj, orderU::data_interface* t)
{
    jlong handle = reinterpret_cast<jlong>(t);
    env->SetLongField(obj, getHandleField(env, obj), handle);
}

template <typename T>
std::vector<std::shared_ptr<T>> toVector(JNIEnv *env, jobjectArray items)
{
    std::vector<std::shared_ptr<T>> ret;
    for (int i = 0; i < env->GetArrayLength(items); i++) {
        jobject obj = (jobject) env->GetObjectArrayElement(items, i);
        auto handle = getHandle<T>(env, obj);
        if(handle)
            ret.push_back(handle);
    }
    return ret;
}

template<typename T>
jobjectArray toJObjectArray(JNIEnv *env, const char*jclass, std::vector<std::shared_ptr<T>> items)
{
    jobject obj = 0;
    jobjectArray ret= (jobjectArray)env->NewObjectArray(items.size(),env->FindClass(jclass), obj);

    for(int i = 0; i < items.size(); i++)
    {
        auto adapterClass = env->FindClass(jclass);
        jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
        std::shared_ptr<T> *ptr = new std::shared_ptr<T>(items[i]);
        jlong handle = reinterpret_cast<jlong>(ptr);
        jobject employeeObject = env->NewObject(adapterClass, midConstructor, handle);
        env->SetObjectArrayElement(ret,i,employeeObject);
    }
    return ret;
}

template<typename T>
jobject toJObject(JNIEnv *env, std::shared_ptr<T> obj, const char* clazz)
{
    std::shared_ptr<T> *item_ptr = new std::shared_ptr<T>(obj);

    jclass adapterClass = env->FindClass(clazz);
    jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
    jlong handle = reinterpret_cast<jlong>(item_ptr);
    return env->NewObject(adapterClass, midConstructor, handle);
}

std::string toString(JNIEnv *env, jstring js)
{
    const jclass stringClass = env->GetObjectClass(js);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(js, getBytes, env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);
    std::string ret = std::string((char *)pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}


#endif //ORDERU_HELPERS_H
