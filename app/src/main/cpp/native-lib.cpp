#include <jni.h>
#include <string>
#include <functional>
#include <thread>
#include <memory>

#include "ClientHandler.h"

#include "helpers.h"


using namespace orderU;


static std::shared_ptr<ClientHandler> ch = nullptr;

/*LIBRARY HANDLER START*/
extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_example_orderu_lib_CManager_getTables(JNIEnv *env, jclass clazz) {

    return toJObjectArray(env, "com/example/orderu/lib/CTable", ch->_client->getTableManager()->get_list());
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_orderu_lib_CManager_performLogin(JNIEnv *env, jclass clazz, jint code) {

    return ch->_client->login(code);

}extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_orderu_lib_CManager_startClient(JNIEnv *env, jclass clazz, jstring ip) {
    ch->init(toString(env, ip).c_str());
    ch->start();
    return (jboolean)!ch->_client->stopped();
}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_example_orderu_lib_CManager_getItems(JNIEnv *env, jclass clazz) {

    auto& items = ch->_client->getItemManager()->get_list();
    return toJObjectArray(env, "com/example/orderu/lib/CItem", items);
}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_example_orderu_lib_CManager_getBills(JNIEnv *env, jclass clazz) {

    return toJObjectArray(env,"com/example/orderu/lib/CBill", ch->_client->getBillManager()->get_list());
}

/*LIBRARY HANDLER END*/

/*-------------------------------------------------------------------------------------------*/

/*ORDERU CLIENT START*/

extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_lib_COrderuClient_addOrderList(JNIEnv *env, jclass clazz,
                                                       jobjectArray items, jint table) {
    std::vector<std::shared_ptr<item>> ritems = toVector<item>(env, items);

    std::vector<order> orders;
    for(auto &i : ritems)
    {
        orders.push_back(order(i));
    }

    ch->_client->addOrderList(orders, table);

}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_lib_COrderuClient_addBill(JNIEnv *env, jclass clazz,
                                                      jobjectArray orders, jint table) {
    std::vector<std::shared_ptr<order>> ritems = toVector<order>(env, orders);
    bill _bill(ritems, table);
    ch->_client->addBill(_bill);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_lib_COrderuClient_addItemList(JNIEnv *env, jclass clazz,
                                                      jobjectArray items) {
    std::vector<std::shared_ptr<item>> ritems = toVector<item>(env, items);
    std::vector<item> tems;
    for(auto &item : ritems)
        tems.push_back(*item.get());
    ch->_client->addItemList(tems);
}

/*ORDERU CLIENT END*/

/*-------------------------------------------------------------------------------------------*/

/*CITEMInterface START*/

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_orderu_lib_CItemInterface_getId(JNIEnv *env, jobject thiz) {
    auto p = getHandle<data_interface>(env, thiz);
    return env->NewStringUTF(serializer::serialize(p->getId()).c_str());
}
extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_orderu_lib_CItemInterface_getName(JNIEnv *env, jobject thiz) {
    auto p = getHandle<data_interface>(env, thiz);
    return env->NewStringUTF(p->getName().c_str());
}


extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_lib_CItemInterface_setName(JNIEnv *env, jobject thiz, jstring name) {
    auto p = getHandle<data_interface>(env, thiz);
    p->setName(toString(env,name));
}

/*CITEMInterface END*/

/*-------------------------------------------------------------------------------------------*/

/*CORDER START*/

extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_orderu_lib_COrder_getItem(JNIEnv *env, jobject thiz) {
    auto p = getHandle<order>(env, thiz);
    jobject obj = 0;
    jclass adapterClass = env->FindClass("com/example/orderu/lib/CItem");
    jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
    std::shared_ptr<item> *ptr = new std::shared_ptr<item>();
    *ptr =  p->getItem();
    jlong handle = reinterpret_cast<jlong>(ptr);
    jobject employeeObject = env->NewObject(adapterClass, midConstructor, handle);
    return employeeObject;
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_orderu_lib_COrder_newOrder(JNIEnv *env, jclass clazz) {
    std::shared_ptr<order> *item_ptr = new std::shared_ptr<order>(std::make_shared<order>());

    jclass adapterClass = env->FindClass("com/example/orderu/lib/COrder");
    jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
    jlong handle = reinterpret_cast<jlong>(item_ptr);
    return env->NewObject(adapterClass, midConstructor, handle);
}


/*CORDER END*/

/*-------------------------------------------------------------------------------------------*/

/*CITEM START*/
extern "C"
JNIEXPORT jfloat JNICALL
Java_com_example_orderu_lib_CItem_getPrice(JNIEnv *env, jobject thiz) {
    auto p = getHandle<item>(env, thiz);
    return p->getPrice();
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_orderu_lib_CItem_newItem(JNIEnv *env, jclass clazz) {
    std::shared_ptr<item> *item_ptr = new std::shared_ptr<item>(std::make_shared<item>());

    jclass adapterClass = env->FindClass("com/example/orderu/lib/CItem");
    jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
    jlong handle = reinterpret_cast<jlong>(item_ptr);
    return env->NewObject(adapterClass, midConstructor, handle);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_lib_CItem_setPrice(JNIEnv *env, jobject thiz, jfloat price) {
    auto p = getHandle<item>(env, thiz);
    p->setPrice(price);
}


/*CITEM END*/

/*-------------------------------------------------------------------------------------------*/

/*CBILL START*/

extern "C"
JNIEXPORT jfloat JNICALL
Java_com_example_orderu_lib_CBill_getTotal(JNIEnv *env, jobject thiz) {
    auto p = getHandle<bill>(env, thiz);
    return p->getTotal();
}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_example_orderu_lib_CBill_getItems(JNIEnv *env, jobject thiz) {

    auto p = getHandle<bill>(env, thiz);
    return toJObjectArray(env, "com/example/orderu/lib/COrder",p->getOrders());
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_orderu_lib_CBill_getTime(JNIEnv *env, jobject thiz) {
    auto p = getHandle<bill>(env, thiz);
    return p->getTime();
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_orderu_lib_CBill_getTable(JNIEnv *env, jobject thiz) {
    auto p = getHandle<bill>(env, thiz);
    return p->getTable();
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_orderu_lib_CBill_newBill(JNIEnv *env, jclass clazz) {
    std::shared_ptr<bill> *item_ptr = new std::shared_ptr<bill>(std::make_shared<bill>());

    jclass adapterClass = env->FindClass("com/example/orderu/lib/CBill");
    jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
    jlong handle = reinterpret_cast<jlong>(item_ptr);
    return env->NewObject(adapterClass, midConstructor, handle);
}

/*CBILL END*/

/*-------------------------------------------------------------------------------------------*/
extern "C"
JNIEXPORT jint JNICALL
Java_com_example_orderu_lib_CTable_getNumber(JNIEnv *env, jobject thiz) {
    auto p = getHandle<table>(env, thiz);
    return p->getNumber();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_lib_CTable_setNumber(JNIEnv *env, jobject thiz, jdouble number) {
    auto p = getHandle<table>(env, thiz);
    p->setNumber(number);
}
extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_example_orderu_lib_CTable_getOrders(JNIEnv *env, jobject thiz) {
    auto p = getHandle<table>(env, thiz);
    return toJObjectArray(env, "com/example/orderu/lib/COrder",p->getOrders());
}extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_orderu_lib_CTable_newTable(JNIEnv *env, jclass clazz, jint number) {

    std::shared_ptr<table> *item_ptr = new std::shared_ptr<table>(std::make_shared<table>(number));

    jclass adapterClass = env->FindClass("com/example/orderu/lib/CTable");
    jmethodID midConstructor = env->GetMethodID(adapterClass, "<init>", "(J)V");
    jlong handle = reinterpret_cast<jlong>(item_ptr);
    return env->NewObject(adapterClass, midConstructor, handle);
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_orderu_lib_CWaiter_hasPermission(JNIEnv *env, jclass clazz, jint up, jint ut) {
    return ch->_client->getWaiter().hasPermission((user_permission )up, (permission_type)ut);
}
extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_orderu_lib_CClientStatus_isConnected(JNIEnv *env, jclass clazz) {
    return (jboolean)ch->isConnected();
}extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_orderu_lib_CManager_retryConnect(JNIEnv *env, jclass clazz) {
    ch->_client->reset("10.0.2.2");
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    return static_cast<jboolean>(ch->isConnected());
}

static JavaVM *jvm = nullptr;
static jclass cclientinfo = NULL;
static jmethodID setClientStatusMethodID = NULL;


JNIEnv *getEnv()
{
    JNIEnv *env;
    // double check it's all ok
    int getEnvStat = jvm->GetEnv((void **)&env, JNI_VERSION_1_6);
    if (getEnvStat == JNI_EDETACHED) {
        LOG("GetEnv: not attached");
        if (jvm->AttachCurrentThread(&env, NULL) != 0) {
            LOG("Failed to attach");
            return nullptr;
        }
    } else if (getEnvStat == JNI_OK) {
        //
    } else if (getEnvStat == JNI_EVERSION) {
        LOG("GetEnv: version not supported");
        return nullptr;
    }
    return env;
}

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    jvm = vm;
    JNIEnv *env = getEnv();
    if (!env) {
        return JNI_ERR; // JNI version not supported.
    }
    ch = std::make_shared<ClientHandler>();
    // Find thread's context class loader.
    cclientinfo = env->FindClass("com/example/orderu/lib/CClientStatus");
    assert(cclientinfo != NULL);
    cclientinfo = (jclass)env->NewGlobalRef(cclientinfo);
    assert(cclientinfo != NULL);

    ch->_client->getService()->onConnect.push_back([&](const boost::system::error_code & e)
   {
       if(!e)
       {
           env = getEnv();

           setClientStatusMethodID = env->GetStaticMethodID(cclientinfo, "setClientStatus", "(Z)V");
           assert(setClientStatusMethodID != NULL);
           env->CallStaticVoidMethod(cclientinfo, setClientStatusMethodID, true);
       }
   });

    ch->_client->getService()->onDisconnect.push_back([&](connection_ptr conn)
  {
      env = getEnv();

      setClientStatusMethodID = env->GetStaticMethodID(cclientinfo, "setClientStatus", "(Z)V");
      assert(setClientStatusMethodID != NULL);
      env->CallStaticVoidMethod(cclientinfo, setClientStatusMethodID, false);
  });
    LOG("INTIALIZED JNI");
    return JNI_VERSION_1_6;
}





extern "C"
JNIEXPORT void JNICALL
Java_com_example_orderu_MainActivity_init(JNIEnv *env, jclass clazz) {

}